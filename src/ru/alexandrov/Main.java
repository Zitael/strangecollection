package ru.alexandrov;

public class Main {
    public static void main(String[] args) {
        StrangeCollection list = new StrangeCollection();
        list.add(1);
        list.add(246);
        list.add(238);
        list.add(-345);
        list.add(345);
        list.add(385);
        System.out.println("Базовый массив: ");
        System.out.println(list);
        System.out.println("Размер массива: ");
        System.out.println(list.size());
        int[] array = {5, 6, 7, 8, 9};
        list.addAll(array);
        System.out.println("Добавили подмассив: ");
        System.out.println(list);
        ;
        System.out.println("Проверяем индексы: ");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(i + " - " + list.get(i));

        }
    }
}