package ru.alexandrov;

public class StrangeCollection {
    private final int INIT_SIZE = 5;
    private final double loadFactor = 0.75;
    private int size = 0;
    private int[] values = new int[INIT_SIZE];

    public int size() {
        return this.size;
    }

    public void add(int value) {
        if ((double) size / values.length > loadFactor) resize();
        values[size++] = (int) (value * Math.random());
    }

    public void addAll(int[] array) {
        for (int i : array) {
            add(i);
        }
    }

    private void resize() {
        int[] newArray = new int[values.length * 2];
        System.arraycopy(values, 0, newArray, 0, values.length);
        values = newArray;
    }

    public int get(int index) {
        if (index >= 0) return values[index];
        else return -1;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("[");
        for (int i = 0; i < this.size; i++) {
            if (i != 0) result.append(", ");
            result.append(values[i]);
        }
        result.append("]");
        return result.toString();
    }
}
